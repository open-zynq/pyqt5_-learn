# PyQt5_Motor

<div align="center">
  <p>
    <img width="284" src="https://gitee.com/open-zynq/pyqt5_-learn/tree/master/README.assets/1.png">
  </p>

[文档（国内）](https://gitee.com/open-zynq/pyqt5_-learn/blob/master/README.md) |
[GUI程序](https://aisuda.github.io/amis-editor-demo/) |
[源码](https://gitee.com/open-zynq/pyqt5_-learn.git) |

</div>

<div align="center">
  交流群：暂无
</div>

<div align="center">

![build](https://img.shields.io/badge/build-passing-{徽标颜色}.svg)
![license](https://img.shields.io/badge/license-C-{徽标颜色}.svg)
![version](https://img.shields.io/badge/version-1.1-red.svg)
![language](https://img.shields.io/badge/Python-100-blue.svg)

</div>



#### 介绍
用PyQt5制作的一个上位机软件，用来控制国创项目的电机。

![2](README.assets/2.png)
![3](README.assets/3.png)


#### 开发环境

**系统：** windows10

**开发软件：** Pycharm 2021.3.3

**Python版本：** python3.10

**QT版本：** PyQt5



#### 使用说明

无

#### 友情链接

本项目其他代码地址：https://gitee.com/open-zynq/guozharanliaozhibei

设计时参考的代码地址：https://gitee.com/tangbeida/PyQt_Arduino




