# 导入库文件
import ast
import sys, os

if hasattr(sys, 'frozen'):
    os.environ['PATH'] = sys._MEIPASS + ";" + os.environ['PATH']
import time
import webbrowser
from PyQt5.QtCore import QTimer, QByteArray
from PyQt5.QtWidgets import *
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
from PyQt5.QtWidgets import QApplication, QMainWindow
from Fuel import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    # 初始化
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        # 设置实例
        self.CreateItems()
        # 设置信号和槽
        self.CreateSignalSlot()
        self.count = 0

    # 设置实例
    def CreateItems(self):
        # QT串口类
        self.com = QSerialPort()
        # QT定时器类
        self.timer = QTimer(self)  # 初始化一个定时器
        self.timer.timeout.connect(self.ShowTime)  # 计时结束调用operate()方法
        self.timer.start(100)  # 设置计时间隔 100ms 并启动

    # 设置信号与槽
    def CreateSignalSlot(self):
        self.pushButton.clicked.connect(self.pushButton_clicked)  # 串口刷新
        self.actionAbout.triggered.connect(self.Goto_MyBlog)  # 关于我们
        self.actions.triggered.connect(self.Goto_friend)  # 友情链接
        self.pushButton_2.clicked.connect(self.Com_Open)  # 打开串口
        self.pushButton_18.clicked.connect(self.Com_Close)  # 关闭串口
        self.pushButton_20.clicked.connect(self.ComSendData)  # 发送数据
        self.com.readyRead.connect(self.Com_Receive_Data)  # 接收数据
        self.horizontalSlider_M1.valueChanged.connect(self.Slider_M1)  # 拖动控件-控制底座舵机
        self.horizontalSlider_M23.valueChanged.connect(self.Slider_M23)  # 拖动控件-控制左舵机
        self.horizontalSlider_M4.valueChanged.connect(self.Slider_M4)  # 拖动控件-控制右舵机
        self.horizontalSlider_M5.valueChanged.connect(self.Slider_M5)  # 拖动控件-控制爪子舵机
        self.horizontalSlider_M6.valueChanged.connect(self.Slider_M6)  # 拖动控件-未定义

        self.horizontalSlider_Timer.valueChanged.connect(self.Slider_Timer)  # 拖动控件-未定义

        self.pushButton_10.clicked.connect(self.Add_action)  # 添加动作按钮
        self.pushButton_11.clicked.connect(self.Del_action)  # 删除动作按钮
        self.pushButton_14.clicked.connect(self.Save_action)  # 保存动作按钮
        self.pushButton_13.clicked.connect(self.Download_action)  # 下载动作组按钮
        self.pushButton_16.clicked.connect(self.Run_action)  # 运行动作组按钮
        self.pushButton_15.clicked.connect(self.Open_action)  # 打开动作组按钮

        self.pushButton_M1.clicked.connect(self.Button_M1)
        self.pushButton_M23.clicked.connect(self.Button_M23)
        self.pushButton_M4.clicked.connect(self.Button_M4)
        self.pushButton_M5.clicked.connect(self.Button_M5)
        self.pushButton_M6.clicked.connect(self.Button_M6)
        self.checkBox_M7.stateChanged.connect(self.Box_M7)
        self.checkBox_M8.stateChanged.connect(self.Box_M8)
        self.checkBox_M9.stateChanged.connect(self.Box_M9)
        self.checkBox_M10.stateChanged.connect(self.Box_M10)
        self.checkBox_M11.stateChanged.connect(self.Box_M11)

        self.pushButton_Timer.clicked.connect(self.Button_Timer)
        self.pushButton_TimerContinue.clicked.connect(self.Button_TimerContinue)
        self.pushButton_TimerPause.clicked.connect(self.Button_TimerPause)
        self.pushButton_TimerStop.clicked.connect(self.Button_TimerStop)

        # 显示时间
    def ShowTime(self):
        self.label_11.setText(time.strftime("%m %d, %H:%M:%S", time.localtime()))

    # 跳转到我的博客，请勿改动
    def Goto_MyBlog(self):
        webbrowser.open("https://gitee.com/open-zynq/")

    # 跳转到友情链接
    def Goto_friend(self):
        webbrowser.open("https://gitee.com/open-zynq/guozharanliaozhibei/")

    # 串口发送数据
    def ComSendData(self):
        SendData = self.textEdit_9.toPlainText()
        if len(SendData) == 0:
            return
        else:
            for i in range(0, len(SendData) - 17, 18):
                m = i + 18
                sendData = SendData[i:m]
                self.com.write(sendData.encode('UTF-8'))

    # 串口接收数据
    def Com_Receive_Data(self):
        try:
            rxData = bytes(self.com.readAll())
            self.textEdit_8.insertPlainText(rxData.decode('gb2312'))
            self.count += 1
            if self.count > 15:
                self.count = 0
                self.textEdit_8.clear()
        except:
            QMessageBox.critical(self, 'ERROR', '串口接收数据错误')
            return

    # 串口刷新
    def pushButton_clicked(self):
        self.comboBox_2.clear()
        com = QSerialPort()
        com_list = QSerialPortInfo.availablePorts()
        for info in com_list:
            com.setPort(info)
            if com.open(QSerialPort.ReadWrite):
                self.comboBox_2.addItem(info.portName())
                com.close()

    # 串口打开按钮按下
    def Com_Open(self):
        comName = self.comboBox_2.currentText()
        comBaud = int(self.comboBox.currentText())
        self.com.setPortName(comName)
        self.com.setBaudRate(comBaud)
        try:
            if not self.com.open(QSerialPort.ReadWrite):
                QMessageBox.critical(self, 'ERROR', '串口打开失败')
                return
        except:
            QMessageBox.critical(self, 'ERROR', '串口打开失败')
            return
        self.pushButton_18.setEnabled(True)
        self.pushButton_2.setEnabled(False)
        self.pushButton.setEnabled(False)
        self.comboBox_2.setEnabled(False)
        self.comboBox.setEnabled(False)

        self.pushButton_19.setEnabled(True)
        self.pushButton_20.setEnabled(True)

        self.pushButton_M1.setEnabled(True)
        self.pushButton_M23.setEnabled(True)
        self.pushButton_M4.setEnabled(True)
        self.pushButton_M5.setEnabled(True)
        self.pushButton_M6.setEnabled(True)
        self.checkBox_M7.setEnabled(True)
        self.checkBox_M8.setEnabled(True)
        self.checkBox_M9.setEnabled(True)
        self.checkBox_M10.setEnabled(True)
        self.checkBox_M11.setEnabled(True)

        self.pushButton_Timer.setEnabled(True)
        self.pushButton_TimerStop.setEnabled(True)
        self.pushButton_TimerPause.setEnabled(True)
        self.pushButton_TimerContinue.setEnabled(True)

        self.pushButton_14.setEnabled(True)
        self.pushButton_11.setEnabled(True)
        self.pushButton_16.setEnabled(True)
        self.pushButton_12.setEnabled(True)
        self.pushButton_15.setEnabled(True)
        self.pushButton_13.setEnabled(True)
        self.pushButton_17.setEnabled(True)
        self.pushButton_10.setEnabled(True)

    # 串口关闭按钮按下
    def Com_Close(self):
        self.com.close()
        # 让控件处于不可选状态
        self.pushButton_18.setEnabled(False)
        self.pushButton_2.setEnabled(True)
        self.pushButton.setEnabled(True)
        self.comboBox_2.setEnabled(True)
        self.comboBox.setEnabled(True)

        self.pushButton_19.setEnabled(False)
        self.pushButton_20.setEnabled(False)

        self.pushButton_M1.setEnabled(False)
        self.pushButton_M23.setEnabled(False)
        self.pushButton_M4.setEnabled(False)
        self.pushButton_M5.setEnabled(False)
        self.pushButton_M6.setEnabled(False)
        self.checkBox_M7.setEnabled(False)
        self.checkBox_M8.setEnabled(False)
        self.checkBox_M9.setEnabled(False)
        self.checkBox_M10.setEnabled(False)
        self.checkBox_M11.setEnabled(False)

        self.pushButton_Timer.setEnabled(False)
        self.pushButton_TimerStop.setEnabled(False)
        self.pushButton_TimerPause.setEnabled(False)
        self.pushButton_TimerContinue.setEnabled(False)

        self.pushButton_14.setEnabled(False)
        self.pushButton_11.setEnabled(False)
        self.pushButton_16.setEnabled(False)
        self.pushButton_12.setEnabled(False)
        self.pushButton_15.setEnabled(False)
        self.pushButton_13.setEnabled(False)
        self.pushButton_17.setEnabled(False)
        self.pushButton_10.setEnabled(False)



    # 拖动控件-控制底座舵机
    def Slider_M1(self):
        Data = self.horizontalSlider_M1.value()
        self.textEdit_10.setText(str(Data))

    def Button_M1(self):
        SendData = [0xfd, 0x01, 0, 0, 0xfe]
        Data = self.horizontalSlider_M1.value()
        Directional = int(self.comboBox_M1.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    # 拖动控件-控制左舵机
    def Slider_M23(self):
        Data = self.horizontalSlider_M23.value()
        self.textEdit_2.setText(str(Data))

    def Button_M23(self):
        SendData = [0xfd, 0x02, 0, 0, 0xfe]
        Data = self.horizontalSlider_M23.value()
        Directional = int(self.comboBox_M23.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    # 拖动控件-控制右舵机
    def Slider_M4(self):
        Data = self.horizontalSlider_M4.value()
        self.textEdit_3.setText(str(Data))

    def Button_M4(self):
        SendData = [0xfd, 0x04, 0, 0, 0xfe]
        Data = self.horizontalSlider_M4.value()
        Directional = int(self.comboBox_M4.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    # 拖动控件-控制爪子舵机
    def Slider_M5(self):
        Data = self.horizontalSlider_M5.value()
        self.textEdit_4.setText(str(Data))

    def Button_M5(self):
        SendData = [0xfd, 0x05, 0, 0, 0xfe]
        Data = self.horizontalSlider_M5.value()
        Directional = int(self.comboBox_M5.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    # 拖动控件-控制下部
    def Slider_M6(self):
        Data = self.horizontalSlider_M6.value()
        self.textEdit_5.setText(str(Data))

    def Button_M6(self):
        SendData = [0xfd, 0x06, 0, 0, 0xfe]
        Data = self.horizontalSlider_M6.value()
        Directional = int(self.comboBox_M6.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    def Slider_Timer(self):
        Data = self.horizontalSlider_Timer.value()
        self.textEdit_Timer.setText(str(Data))

    def Button_Timer(self):
        SendData = [0xfd, 0x06, 0, 0, 0xfe]
        Data = self.horizontalSlider_M6.value()
        Directional = int(self.comboBox_M6.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return
    def Button_TimerStop(self):
        SendData = [0xfd, 0x06, 0, 0, 0xfe]
        Data = self.horizontalSlider_M6.value()
        Directional = int(self.comboBox_M6.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return
    def Button_TimerContinue(self):
        SendData = [0xfd, 0x06, 0, 0, 0xfe]
        Data = self.horizontalSlider_M6.value()
        Directional = int(self.comboBox_M6.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    def Button_TimerPause(self):
        SendData = [0xfd, 0x06, 0, 0, 0xfe]
        Data = self.horizontalSlider_M6.value()
        Directional = int(self.comboBox_M6.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    def Box_M7(self):
        SendData = [0xfd, 0x07, 0, 0, 0xfe]
        Data = int(self.checkBox_M7.isChecked())
        Directional = int(self.comboBox_M7.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    def Box_M8(self):
        SendData = [0xfd, 0x08, 0, 0, 0xfe]
        Data = int(self.checkBox_M8.isChecked())
        Directional = int(self.comboBox_M8.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    def Box_M9(self):
        SendData = [0xfd, 0x09, 0, 0, 0xfe]
        Data = int(self.checkBox_M9.isChecked())
        Directional = int(self.comboBox_M9.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    def Box_M10(self):
        SendData = [0xfd, 10, 0, 0, 0xfe]
        Data = int(self.checkBox_M10.isChecked())
        Directional = int(self.comboBox_M10.currentText())
        try:
            SendData[2] = Directional
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    def Box_M11(self):
        SendData = [0xfd, 11, 0, 0, 0xfe]
        Data = int(self.checkBox_M11.isChecked())
        try:
            SendData[3] = Data
            if len(SendData) == 0:
                return
            else:
                print(SendData)
                for num in SendData:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
        except:
            print("error\r\n")
            return

    # 添加动作按钮
    def Add_action(self):
        SendData = [0xaa, 0x40, 0, 0, 0, 0, 0xff]
        temp_1 = int(self.horizontalSlider.value())
        temp_2 = int(self.horizontalSlider_2.value())
        temp_3 = int(self.horizontalSlider_3.value())
        temp_4 = int(self.horizontalSlider_4.value())

        SendData[2] = temp_1
        SendData[3] = temp_2
        SendData[4] = temp_3
        SendData[5] = temp_4

        Data = str(SendData) + '\n'
        self.textEdit_7.insertPlainText(Data)

    # 保存动作组
    def Save_action(self):
        try:
            f = open('text', 'a')
            SendData = self.textEdit_7.toPlainText()
            if len(SendData) > 0:
                f.write(SendData)
                f.close()
                QMessageBox.about(self, "提示", "保存成功")
            else:
                QMessageBox.critical(self, "ERROR", "未添加动作")
        except:
            QMessageBox.critical(self, "ERROR", "操作失败")

    # 删除动作组
    def Del_action(self):
        try:
            open('text', 'w').close()
            QMessageBox.about(self, "提示", "删除成功")
        except:
            QMessageBox.critical(self, "ERROR", "操作失败")

    # 打开动作组
    def Open_action(self):
        try:
            f = open('text', 'r')
            Text_action = f.read()
            self.textEdit_7.insertPlainText(Text_action)
            f.close()
        except:
            pass

    # 下载动作组
    def Download_action(self):
        pass

    # 运行动作组
    def Run_action(self):
        SendData = self.textEdit_7.toPlainText()
        if len(SendData) == 0:
            QMessageBox.critical(self, "ERROR", "动作组为空")
        else:
            sendDataBlocks = SendData.split("\n")  # 使用"\n"分割SendData
            print(sendDataBlocks)
            for i in range(len(sendDataBlocks) - 1):
                int_list = ast.literal_eval(sendDataBlocks[i])
                print(int_list)
                for num in int_list:
                    hex_string = hex(num)[2:]
                    self.com.write(QByteArray.fromHex(hex_string.encode()))
                    print(hex_string)


def main():
    window = QApplication(sys.argv)
    TheWin = MainWindow()
    TheWin.show()
    sys.exit(window.exec_())


if __name__ == '__main__':
    main()
